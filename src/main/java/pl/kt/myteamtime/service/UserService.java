package pl.kt.myteamtime.service;

import pl.kt.myteamtime.dto.UserDTO;
import pl.kt.myteamtime.entity.User;
import pl.kt.myteamtime.entity.VerificationToken;
import pl.kt.myteamtime.validation.EmailExistsException;

import java.util.List;

public interface UserService {
    User registerNewUserAccount(UserDTO accountDto) throws EmailExistsException;

    void createVerificationToken(User user, String token);

    VerificationToken getVerificationToken(String token);

    void saveRegisteredUser(User user);

    User getUser(String verificationToken);

    User findByIdentificator(String userIdentificator);

    VerificationToken generateNewVerificationToken(String existingToken);

    User findUserByEmail(String userEmail);

    User findUserByIdentificator(String userIdentificator);

    void createPasswordResetTokenForUser(User user, String token);

    public void changeUserPassword(User user, String password);

    boolean checkIfValidOldPassword(User user, String oldPassword);

    void saveUser(User user);

    List<UserDTO> findAll();

}
