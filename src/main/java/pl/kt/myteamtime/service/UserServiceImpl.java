package pl.kt.myteamtime.service;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import pl.kt.myteamtime.dto.UserDTO;
import pl.kt.myteamtime.entity.PasswordResetToken;
import pl.kt.myteamtime.entity.Role;
import pl.kt.myteamtime.entity.User;
import pl.kt.myteamtime.entity.VerificationToken;
import pl.kt.myteamtime.mapper.CycleAvoidingMappingContext;
import pl.kt.myteamtime.mapper.UserMapper;
import pl.kt.myteamtime.repository.PasswordTokenRepository;
import pl.kt.myteamtime.repository.TokenRepository;
import pl.kt.myteamtime.repository.UserRepository;
import pl.kt.myteamtime.validation.EmailExistsException;
import pl.kt.myteamtime.validation.ValidUserNoPasswordException;

@Service
public class UserServiceImpl implements UserService {

	@Autowired
	private UserRepository userRepository;

	@Autowired
	private TokenRepository tokenRepository;

	@Autowired
	private UserMapper userMapper;

	@Autowired
	private PasswordEncoder passwordEncoder;

	@Autowired
	private PasswordTokenRepository passwordTokenRepository;

	@Autowired
	private EmployeeService employeeService;

	@Override
	public User registerNewUserAccount(UserDTO accountDto) throws EmailExistsException {

		if (emailExist(accountDto.getEmail())) {
			throw new EmailExistsException("There is an account with that email address: " + accountDto.getEmail());
		}

		accountDto.setEnabled(false); // enabled means that account is validated by User
		accountDto.setActive(true); // active means that account is not expired
		User newUser = new User();
		List<String> roleNames = new ArrayList<>();
		List<Role> roles = new ArrayList<>();
		roleNames.add(accountDto.getRole1());
		roleNames.add(accountDto.getRole2());
		roleNames.add(accountDto.getRole3());
		roleNames.add(accountDto.getRole4());
		roleNames.add(accountDto.getRole5());
		for (String r : roleNames) {

			if (r != null) {
				Role role = new Role();
				role.setName(r);
				roles.add(role);

			}
		}
		newUser.setRoles(roles);
		newUser = userRepository.save(userMapper.mapToUser(accountDto, new CycleAvoidingMappingContext()));
		// newUser.setPassword(passwordEncoder.encode(accountDto.getPassword()));
		return newUser;
	}

	@Override
	public void createVerificationToken(User user, String token) {
		VerificationToken myToken = new VerificationToken(token, user);
		tokenRepository.save(myToken);
	}

	@Override
	public VerificationToken getVerificationToken(String token) {
		return tokenRepository.findByToken(token);

	}

	@Override
	public void saveRegisteredUser(User user) {
		if (userRepository.findActiveByIdentificator(user.getIdentificator()) != null) {

			User foundUser = userRepository.findActiveByIdentificator(user.getIdentificator());
			if (foundUser.isEnabled()) {
				if (foundUser.getPassword() == null) {
					throw new ValidUserNoPasswordException();
				}
			}
		}

		employeeService.saveEmployee(user);
		userRepository.save(user);
	}

	@Override
	public User getUser(String verificationToken) {
		return tokenRepository.findByToken(verificationToken).getUser();
	}

	@Override
	public User findByIdentificator(String userIdentificator) {
		return userRepository.findActiveByIdentificator(userIdentificator);
	}

	@Override
	public VerificationToken generateNewVerificationToken(String existingToken) {
		VerificationToken vToken = tokenRepository.findByToken(existingToken);
		vToken.updateToken(UUID.randomUUID().toString());
		vToken = tokenRepository.save(vToken);
		return vToken;
	}

	@Override
	public User findUserByEmail(String userEmail) {
		return userRepository.findActiveByEmail(userEmail);
	}

	@Override
	public User findUserByIdentificator(String userIdentificator) {
		return userRepository.findActiveByIdentificator(userIdentificator);
	}

	private boolean emailExist(String email) {
		User user = userRepository.findActiveByEmail(email);
		if (user != null) {
			return true;
		}
		return false;
	}

	public void createPasswordResetTokenForUser(User user, String token) {
		PasswordResetToken myToken = new PasswordResetToken(token, user);
		passwordTokenRepository.save(myToken);
	}

	@Override
	public void changeUserPassword(User user, String password) {
		user.setPassword(passwordEncoder.encode(password));
		userRepository.save(user);
	}

	@Override
	public boolean checkIfValidOldPassword(User user, String oldPassword) {
		return passwordEncoder.matches(oldPassword, user.getPassword());
	}

	@Override
	public void saveUser(User user) {

		userRepository.save(user);
	}

	@Override
	public List<UserDTO> findAll() {

		return userMapper.mapToUserDTOList(userRepository.findAll(), new CycleAvoidingMappingContext());
	}
}
