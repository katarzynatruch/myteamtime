package pl.kt.myteamtime.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import pl.kt.myteamtime.ds.AvailabilitySingleEmployeeUpdateDs;
import pl.kt.myteamtime.dto.AvailabilityDTO;
import pl.kt.myteamtime.dto.EmployeeDTO;
import pl.kt.myteamtime.entity.Availability;
import pl.kt.myteamtime.mapper.AvailabilityMapper;
import pl.kt.myteamtime.mapper.CycleAvoidingMappingContext;
import pl.kt.myteamtime.mapper.EmployeeMapper;
import pl.kt.myteamtime.repository.AvailabilityRepository;

@Service
public class AvailabilityServiceImpl implements AvailabilityService {

	@Autowired
	private AvailabilityMapper availabilityMapper;

	@Autowired
	private AvailabilityRepository availabilityRepository;

	@Autowired
	private EmployeeMapper employeeMapper;

	@Autowired
	private EmployeeService employeeService;

	@Override
	public List<AvailabilityDTO> findByEmployee(EmployeeDTO employee) {

		List<Availability> availabilityList = availabilityRepository
				.findByEmployee(employeeMapper.mapToEmployee(employee, new CycleAvoidingMappingContext()));

		return availabilityMapper.mapToAvailabilityDTOList(availabilityList, new CycleAvoidingMappingContext());
	}

	@Override
	public void save(AvailabilitySingleEmployeeUpdateDs availabilitySingleEmployeeUpdateDs) {

		Availability availability = new Availability();

		for (AvailabilityDTO availabilityDTO : availabilitySingleEmployeeUpdateDs.getAvailability()) {

			availability
					.setEmployee(employeeService.findByEmployeeId(availabilitySingleEmployeeUpdateDs.getEmployeeId()));
			availability.setDay(availabilityDTO.getDay());
			availability.setWeek(availabilityDTO.getWeek());
			availability.setYear(availabilityDTO.getYear());
			availability.setEvent(availabilityDTO.getEvent());
			availabilityRepository.save(availability);
		}

	}

}
