package pl.kt.myteamtime.service;

import java.util.List;

import pl.kt.myteamtime.ds.EmployeeWithProjectAndLimitsListDs;
import pl.kt.myteamtime.ds.EmployeeWithProjectListDs;
import pl.kt.myteamtime.ds.ProjectIdWithMaxHoursDs;
import pl.kt.myteamtime.dto.EmployeeDTO;
import pl.kt.myteamtime.dto.TeamDTO;
import pl.kt.myteamtime.entity.Employee;
import pl.kt.myteamtime.entity.Project;
import pl.kt.myteamtime.entity.User;

public interface EmployeeService {

    EmployeeDTO findByEmployeeIdentificatorDTO(String employeeIdentificator);

    Employee findByEmployeeId(Long employeeId);

    List<EmployeeDTO> findEmployeesByTeamId(Long teamId);

    void saveEmployee(EmployeeDTO employeeDTO);

    List<EmployeeDTO> findEmployeeByManagerIdentificator(String managerIdentificator);

    List<EmployeeWithProjectListDs> findEmployeeWithProjects(String managerIdentificator);

    List<EmployeeDTO> findEmployeeByProjectAndManagerIdentificator(Project project, String managerIdentificator);

    List<Employee> findEmployeesById(List<Long> employeesIdList);

	void saveEmployeeProjectRelations(Long employeeId, List<ProjectIdWithMaxHoursDs> projectIdWithMaxHoursDsList);

    void delete(Long employeeId);

    Employee findByEmployeeIdentificator(String employeeIdentificator);

    void saveEmployee(User user);
}
