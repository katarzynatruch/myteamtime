package pl.kt.myteamtime.service;

public interface DataAndTimeService {
	
	int getCurrentWeekGlobal();
	int getCurrenYearGlobal();
	
	int numberOfWeeksFromDateToDate(int weekFrom, int yearFrom, int weekTo, int yearTo);
	int getMaxWeekOfTheYear(int year);

}
