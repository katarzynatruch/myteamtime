package pl.kt.myteamtime.service;

public interface SecurityService {
    String validatePasswordResetToken(long id, String token);
}
