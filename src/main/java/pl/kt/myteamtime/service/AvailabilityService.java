package pl.kt.myteamtime.service;

import java.util.List;

import pl.kt.myteamtime.ds.AvailabilitySingleEmployeeUpdateDs;
import pl.kt.myteamtime.dto.AvailabilityDTO;
import pl.kt.myteamtime.dto.EmployeeDTO;

public interface AvailabilityService {

	List<AvailabilityDTO> findByEmployee(EmployeeDTO employee);

	void save(AvailabilitySingleEmployeeUpdateDs availabilitySingleEmployeeDs);

}
