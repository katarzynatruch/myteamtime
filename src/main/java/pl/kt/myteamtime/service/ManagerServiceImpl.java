package pl.kt.myteamtime.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import pl.kt.myteamtime.entity.Manager;
import pl.kt.myteamtime.repository.ManagerRepository;

import java.util.Optional;

@Service
public class ManagerServiceImpl implements ManagerService {

	@Autowired
	private
	ManagerRepository managerRepository;

	@Override
	public Manager findByIdentificator(String managerIdentificator) {

		return managerRepository.findByIdentificator(managerIdentificator);
	}

	@Override
	public Manager findById(Long managerId) {
		return managerRepository.findById(managerId).orElse(null);
	}
}
