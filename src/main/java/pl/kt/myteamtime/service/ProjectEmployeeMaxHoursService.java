package pl.kt.myteamtime.service;

import java.util.List;

import pl.kt.myteamtime.dto.EmployeeDTO;
import pl.kt.myteamtime.dto.ProjectDTO;
import pl.kt.myteamtime.dto.ProjectEmployeeMaxHoursDTO;
import pl.kt.myteamtime.entity.Employee;
import pl.kt.myteamtime.entity.Project;

public interface ProjectEmployeeMaxHoursService {

	ProjectEmployeeMaxHoursDTO calculateMaxHoursForProjectAndEmployees(String project, List<String> employees);

	void save(Project project, Employee employee, Integer maxHours);

	long findMaxHoursForProjectAndEmployee(ProjectDTO project, EmployeeDTO employee);


}
