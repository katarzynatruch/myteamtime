package pl.kt.myteamtime.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import pl.kt.myteamtime.ds.ProjectWithEmployeeListDs;
import pl.kt.myteamtime.dto.EmployeeDTO;
import pl.kt.myteamtime.dto.ProjectDTO;
import pl.kt.myteamtime.dts.ProjectListDts;
import pl.kt.myteamtime.entity.Deadline;
import pl.kt.myteamtime.entity.Project;
import pl.kt.myteamtime.enums.ProjectType;
import pl.kt.myteamtime.mapper.CycleAvoidingMappingContext;
import pl.kt.myteamtime.mapper.ProjectMapper;
import pl.kt.myteamtime.repository.ProjectRepository;

@Service
public class ProjectServiceImpl implements ProjectService {

	@Autowired
	private ProjectRepository projectRepository;

	@Autowired
	private EmployeeService employeeService;

	@Autowired
	private ProjectMapper projectMapper;

	@Autowired
	private DeadlineService deadlineService;

	@Override
	public List<ProjectDTO> findProjectsByEmployeeIdentificator(String employeeIdentificator) {

		EmployeeDTO employee = employeeService.findByEmployeeIdentificatorDTO(employeeIdentificator);

		return employee.getProjects();
	}

	@Override
	public ProjectDTO findProjectByProjectNumberDTO(String projectNumber) {

		Project project = projectRepository.findActiveByProjectNumber(projectNumber);

		return projectMapper.mapToProjectDTO(project, new CycleAvoidingMappingContext());
	}

	@Override
	public Project findProjectByProjectNumber(String projectNumber) {

		return projectRepository.findActiveByProjectNumber(projectNumber);
	}

	@Override
	public ProjectListDts loadTabData() {

		List<Project> projects = new ArrayList<Project>();
		projects = projectRepository.findAll();

		ProjectListDts projectListDts = new ProjectListDts();
		projectListDts.setProjects(
				projectMapper.mapToProjectListToProjectDTOList(projects, new CycleAvoidingMappingContext()));

		return projectListDts;
	}

	@Override
	public void saveOrUpdate(ProjectDTO projectDTO) {

		projectDTO.setIsActive(true);
		Project managedProject = projectRepository
				.save(projectMapper.mapToProject(projectDTO, new CycleAvoidingMappingContext()));
		List<Deadline> deadlines = projectDTO.getDeadlines();
		deadlines.forEach(d -> d.setProject(managedProject));
		deadlineService.saveList(deadlines);

	}

	@Override
	public List<ProjectDTO> findAll() {

		List<Project> managedProjects = projectRepository.findAllActive();
		return projectMapper.mapToProjectListToProjectDTOList(managedProjects, new CycleAvoidingMappingContext());
	}

	@Override
	public List<ProjectWithEmployeeListDs> findProjectWithEmployees(String managerIdentificator) {

		List<ProjectWithEmployeeListDs> projectWithEmployeeListDses = new ArrayList<>();
		List<Project> projects = projectRepository.findAllActive();
		for (Project project : projects) {
			ProjectWithEmployeeListDs projectWithEmployeeListDs = new ProjectWithEmployeeListDs();
			List<EmployeeDTO> employees = employeeService.findEmployeeByProjectAndManagerIdentificator(project,
					managerIdentificator);
			projectWithEmployeeListDs.setEmployees(employees);
			projectWithEmployeeListDs
					.setProject(projectMapper.mapToProjectDTO(project, new CycleAvoidingMappingContext()));
			projectWithEmployeeListDses.add(projectWithEmployeeListDs);
		}

		return projectWithEmployeeListDses;
	}

	@Override
	public List<Project> mapProjectsList(List<ProjectDTO> projects) {

		return projectMapper.mapToProjectListToProjectList(projects, new CycleAvoidingMappingContext());
	}

	@Override
	public List<ProjectDTO> findByType(ProjectType type) {

		List<Project> managedProjects = projectRepository.findActiveByType(type);
		return projectMapper.mapToProjectListToProjectDTOList(managedProjects, new CycleAvoidingMappingContext());
	}

	@Override
	public Project findById(Long projectId) {

		return projectRepository.findActiveById(projectId).orElse(null);
	}

    @Override
    public void delete(Long projectId) {

		Optional<Project> project = projectRepository.findById(projectId);
		if (project.isPresent()){
			project.get().setIsActive(false);
			projectRepository.save(project.get());
		}
	}

}
