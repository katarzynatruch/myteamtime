package pl.kt.myteamtime.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import pl.kt.myteamtime.dto.EmployeeDTO;
import pl.kt.myteamtime.dto.ProjectDTO;
import pl.kt.myteamtime.dto.ProjectEmployeeMaxHoursDTO;
import pl.kt.myteamtime.entity.Employee;
import pl.kt.myteamtime.entity.Project;
import pl.kt.myteamtime.entity.ProjectEmployeeMaxHours;
import pl.kt.myteamtime.mapper.CycleAvoidingMappingContext;
import pl.kt.myteamtime.mapper.EmployeeMapper;
import pl.kt.myteamtime.mapper.ProjectEmployeeMaxHoursMapper;
import pl.kt.myteamtime.mapper.ProjectMapper;
import pl.kt.myteamtime.repository.ProjectEmployeeMaxHoursRepository;

@Service
public class ProjectEmployeeMaxHoursServiceImpl implements ProjectEmployeeMaxHoursService {

	@Autowired
	private
	ProjectEmployeeMaxHoursRepository projectEmployeeMaxHoursRepository;

	@Autowired
	ProjectEmployeeMaxHoursMapper projectEmployeeMaxHoursMapper;

	@Autowired
	private
	EmployeeMapper employeeMapper;

	@Autowired
	private
	ProjectMapper projectMapper;
	@Autowired
	private EmployeeService employeeService;
	@Autowired
	private ProjectService projectService;

	@Override
	public ProjectEmployeeMaxHoursDTO calculateMaxHoursForProjectAndEmployees(String projectNumber,
                                                                              List<String> employeeIdentificators) {

		ProjectEmployeeMaxHoursDTO projectEmployeeMaxHoursDTO = new ProjectEmployeeMaxHoursDTO();
		int maxHoursForProject = 0;
		for (String employeeIdentificator : employeeIdentificators) {

			ProjectEmployeeMaxHours currentHours = projectEmployeeMaxHoursRepository.findByEmployeeAndProject(
					employeeService.findByEmployeeIdentificator(employeeIdentificator),
					projectService.findProjectByProjectNumber(projectNumber));

			if (currentHours != null) {
				maxHoursForProject = (int) (maxHoursForProject + currentHours.getMaxHours());
			}

		}

		projectEmployeeMaxHoursDTO.setMaxHours(maxHoursForProject);
		return projectEmployeeMaxHoursDTO;
	}

	@Override
	public void save(Project project, Employee employee, Integer maxHours) {

		ProjectEmployeeMaxHours projectEmployeeMaxHours = new ProjectEmployeeMaxHours();
		projectEmployeeMaxHours.setEmployee(employee);
		projectEmployeeMaxHours.setProject(project);
		projectEmployeeMaxHours.setMaxHours(maxHours);
		projectEmployeeMaxHoursRepository.save(projectEmployeeMaxHours);

	}

	@Override
	public long findMaxHoursForProjectAndEmployee(ProjectDTO project, EmployeeDTO employee) {

		ProjectEmployeeMaxHours maxHours = projectEmployeeMaxHoursRepository.findByEmployeeAndProject(
				employeeMapper.mapToEmployee(employee, new CycleAvoidingMappingContext()),
				projectMapper.mapToProject(project, new CycleAvoidingMappingContext()));
		return maxHours.getMaxHours();
	}

}
