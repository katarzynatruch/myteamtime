package pl.kt.myteamtime.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import pl.kt.myteamtime.ds.EmployeeWithProjectListDs;
import pl.kt.myteamtime.ds.ProjectIdWithMaxHoursDs;
import pl.kt.myteamtime.dto.EmployeeDTO;
import pl.kt.myteamtime.dto.ProjectDTO;
import pl.kt.myteamtime.entity.Employee;
import pl.kt.myteamtime.entity.Project;
import pl.kt.myteamtime.entity.Role;
import pl.kt.myteamtime.entity.User;
import pl.kt.myteamtime.mapper.CycleAvoidingMappingContext;
import pl.kt.myteamtime.mapper.EmployeeMapper;
import pl.kt.myteamtime.mapper.WorkLogMapper;
import pl.kt.myteamtime.repository.EmployeeRepository;

@Service
public class EmployeeServiceImpl implements EmployeeService {

    @Autowired
    private
    EmployeeRepository employeeRepository;

    @Autowired
    private
    EmployeeMapper employeeMapper;

    @Autowired
    WorkLogService workLogService;

    @Autowired
    WorkLogMapper workLogMapper;

    @Autowired
    private ProjectService projectService;

    @Autowired
    private ProjectEmployeeMaxHoursService projectEmployeeMaxHoursService;

    @Autowired
    private UserService userService;

    @Override
    public EmployeeDTO findByEmployeeIdentificatorDTO(String employeeIdentificator) {
        Employee employee = employeeRepository.findActiveByIdentificator(employeeIdentificator);
        if (employee != null) {
            return employeeMapper.mapToEmployeeDTO(employee, new CycleAvoidingMappingContext());
        }
        return null;
    }

    @Override
    public Employee findByEmployeeIdentificator(String employeeIdentificator) {
        Employee employee = employeeRepository.findActiveByIdentificator(employeeIdentificator);
        if (employee != null) {
            return employee;
        }
        return null;
    }

    @Override
    public void saveEmployee(User user) {
       if(employeeRepository.findActiveByIdentificator(user.getIdentificator())==null){
           Employee employee = new Employee();
           employee.setFirstName(user.getFirstName());
           employee.setLastName(user.getLastName());
           employee.setIdentificator(user.getIdentificator());
           employee.setEmail(user.getEmail());
           employee.setIsActive(user.isActive());

           employeeRepository.save(employee);
       }else{
           Employee employee = employeeRepository.findActiveByIdentificator(user.getIdentificator());
           employee.setFirstName(user.getFirstName());
           employee.setLastName(user.getLastName());
           employee.setIdentificator(user.getIdentificator());
           employee.setEmail(user.getEmail());
           employee.setIsActive(user.isActive());

           employeeRepository.save(employee);
       }

    }

    @Override
    public Employee findByEmployeeId(Long employeeId) {

        return employeeRepository.findActiveById(employeeId).orElse(null);
    }

    @Override
    public List<EmployeeDTO> findEmployeesByTeamId(Long teamId) {
        List<Employee> employees = employeeRepository.findActiveEmployeesByTeamsId(teamId);
        return employeeMapper.mapToEmployeeDTOList(employees, new CycleAvoidingMappingContext());

    }

    @Override
    public void saveEmployee(EmployeeDTO employeeDto) {

        if(userService.findUserByIdentificator(employeeDto.getIdentificator())==null){
            User user = new User();
            user.setFirstName(employeeDto.getFirstName());
            user.setLastName(employeeDto.getLastName());
            user.setIdentificator(employeeDto.getIdentificator());
            user.setEmail(employeeDto.getEmail());
            List<Role> roles = new ArrayList<>();
            Role role = new Role();
            role.setName("EMPLOYEE");
            roles.add(role);
            user.setRoles(roles);
            user.setActive(true);
            user.setEnabled(false);
            userService.saveRegisteredUser(user);
        }else{
            User user = userService.findUserByIdentificator(employeeDto.getIdentificator());
            user.setFirstName(employeeDto.getFirstName());
            user.setLastName(employeeDto.getLastName());
            user.setIdentificator(employeeDto.getIdentificator());
            user.setEmail(employeeDto.getEmail());
            user.setEnabled(employeeDto.getIsActive());
            userService.saveRegisteredUser(user);
        }

        saveEmployee(employeeMapper.mapToEmployee(employeeDto, new CycleAvoidingMappingContext()));

    }

    private void saveEmployee(Employee employeeEntity) {

        employeeRepository.save(employeeEntity);
    }

    @Override
    public List<EmployeeDTO> findEmployeeByManagerIdentificator(String managerIdentificator) {

        List<Employee> managedEmployees = employeeRepository.findActiveByManagerIdentificator(managerIdentificator);

        return employeeMapper.mapToEmployeeDTOList(managedEmployees, new CycleAvoidingMappingContext());

    }

    @Override
    public List<EmployeeWithProjectListDs> findEmployeeWithProjects(String managerIdentificator) {

        List<EmployeeWithProjectListDs> employeeWithProjectListDsList = new ArrayList<>();
        List<EmployeeDTO> employees = findEmployeeByManagerIdentificator(managerIdentificator);

        for (EmployeeDTO employeeDTO : employees) {

            EmployeeWithProjectListDs employeeWithProjectListDs = new EmployeeWithProjectListDs();
            List<ProjectDTO> projects = projectService
                    .findProjectsByEmployeeIdentificator(employeeDTO.getIdentificator());

            employeeWithProjectListDs.setEmployee(employeeDTO);
            employeeWithProjectListDs.setProjects(projects);
            employeeWithProjectListDsList.add(employeeWithProjectListDs);

        }

        return employeeWithProjectListDsList;
    }

    @Override
    public List<EmployeeDTO> findEmployeeByProjectAndManagerIdentificator(Project project,
                                                                          String managerIdentificator) {

        List<Employee> employeesByProject = employeeRepository.findActiveByProjectAndManagerIdentificator(project.getId(),
                managerIdentificator);

        return employeeMapper.mapToEmployeeDTOList(employeesByProject, new CycleAvoidingMappingContext());
    }


    @Override
    public List<Employee> findEmployeesById(List<Long> employeesIdList) {

        return employeesIdList.stream().map(this::findById).collect(Collectors.toList());
    }

    @Override
    public void saveEmployeeProjectRelations(Long employeeId,
                                             List<ProjectIdWithMaxHoursDs> projectIdWithMaxHoursDsList) {

        List<Project> projects = new ArrayList<>();
        Employee employee = findById(employeeId);
        for (ProjectIdWithMaxHoursDs p : projectIdWithMaxHoursDsList) {
            Project project = projectService.findById(p.getProjectId());
            if (project != null) {
                projects.add(project);
                if (employee != null) {
                    projectEmployeeMaxHoursService.save(project, employee, p.getMaxHours());
                }
            }

        }

        if (employee != null) {
            employee.setProjects(projects);
            saveEmployee(employee);
        }
    }

    @Override
    public void delete(Long employeeId) {

        Optional<Employee> employee = employeeRepository.findById(employeeId);
        if(employee.isPresent()){
            employee.get().setIsActive(false);
            employeeRepository.save(employee.get());

            User user = userService.findUserByIdentificator(employee.get().getIdentificator());
            user.setActive(false);
            userService.saveUser(user);
        }

    }

    private Employee findById(Long e) {

        return findByEmployeeId(e);
    }

}
