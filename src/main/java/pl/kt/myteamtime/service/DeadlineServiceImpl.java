package pl.kt.myteamtime.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import pl.kt.myteamtime.entity.Deadline;
import pl.kt.myteamtime.repository.DeadlineRepository;

@Service
public class DeadlineServiceImpl implements DeadlineService {

	@Autowired
	private
	DeadlineRepository deadlineRepository;
	
	@Override
	public void saveList(List<Deadline> deadlines) {

		deadlines.forEach(d -> deadlineRepository.save(d));
		
	}



}
