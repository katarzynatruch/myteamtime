package pl.kt.myteamtime.service;

import java.util.List;

import pl.kt.myteamtime.entity.Deadline;

public interface DeadlineService {

	void saveList(List<Deadline> deadlines);

}
