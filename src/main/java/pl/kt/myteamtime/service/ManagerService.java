package pl.kt.myteamtime.service;

import pl.kt.myteamtime.entity.Manager;

public interface ManagerService {

    Manager findByIdentificator(String managerIdentificator);

    Manager findById(Long managerId);
}
