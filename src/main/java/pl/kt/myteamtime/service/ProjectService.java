package pl.kt.myteamtime.service;

import java.util.List;

import pl.kt.myteamtime.ds.ProjectWithEmployeeListDs;
import pl.kt.myteamtime.dto.ProjectDTO;
import pl.kt.myteamtime.dts.ProjectListDts;
import pl.kt.myteamtime.entity.Project;
import pl.kt.myteamtime.enums.ProjectType;

public interface ProjectService {

	List<ProjectDTO> findProjectsByEmployeeIdentificator(String employeeIdentificator);

	ProjectDTO findProjectByProjectNumberDTO(String projectNumber);

	ProjectListDts loadTabData();

	void saveOrUpdate(ProjectDTO projectDTO);

	List<ProjectDTO> findAll();

	List<ProjectWithEmployeeListDs> findProjectWithEmployees(String managerIdentificator);

	List<Project> mapProjectsList(List<ProjectDTO> projects);

	List<ProjectDTO> findByType(ProjectType string);


    Project findById(Long projectId);

    void delete(Long projectId);

	Project findProjectByProjectNumber(String projectNumber);
}
