package pl.kt.myteamtime;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

//@ComponentScan(basePackages = {"src"})
@SpringBootApplication
@ComponentScan(basePackages = {"src", "pl.kt.myteamtime.security"})
public class MyteamtimeApplication {

	public static void main(String[] args) {
		SpringApplication.run(MyteamtimeApplication.class, args);
	}

}
