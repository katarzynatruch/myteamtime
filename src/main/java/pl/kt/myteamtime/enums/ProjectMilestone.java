package pl.kt.myteamtime.enums;

public enum ProjectMilestone {

	AREL("AREL", "A_release"), BREL("BREL", "B_Release"), CREL("CREL", "C_Release"), PREL("PREL", "P_Release"),
	GA("GA", "Gate_A"), GB("GB", "Gate_B"), CP1("CP1", "Checkpoint1"), CP2("CP2", "Checkpoint2");

	private String code;
	private String name;

	private ProjectMilestone(String code, String name) {
		this.code = code;
		this.name = name;
	}

	public String getCode() {
		return code;
	}

	public String getName() {
		return name;
	}

}
