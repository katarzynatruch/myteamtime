package pl.kt.myteamtime.enums;

public enum Roles {

	EMPLOYEE("EMPLOYEE", "Employee"), MANAGER("MANAGER", "Manager"), PMANAGER("PMANAGER", "Project_Manager"),
	ADMIN("ADMIN", "Admin");

	private String code;
	private String name;

	private Roles(String code, String name) {
		this.code = code;
		this.name = name;
	}

	public String getCode() {
		return code;
	}

	public String getName() {
		return name;
	}

}
