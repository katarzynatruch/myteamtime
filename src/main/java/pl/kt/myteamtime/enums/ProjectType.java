package pl.kt.myteamtime.enums;

public enum ProjectType {

	CM("CM", "Common"), RG("RG", "Regular");

	private String code;
	private String name;

	private ProjectType(String code, String name) {
		this.code = code;
		this.name = name;
	}

	public String getCode() {
		return code;
	}

	public String getName() {
		return name;
	}


}
