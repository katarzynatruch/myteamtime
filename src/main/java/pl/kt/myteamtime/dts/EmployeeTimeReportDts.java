package pl.kt.myteamtime.dts;


import lombok.Data;
import pl.kt.myteamtime.ds.WorkLogWeekDs;
import pl.kt.myteamtime.dto.EmployeeDTO;
import pl.kt.myteamtime.dto.ProjectDTO;

import java.util.List;

@Data
public class EmployeeTimeReportDts {

	private List<ProjectDTO> userProjects;
	private EmployeeDTO employee;
	private List<WorkLogWeekDs> workLogWeeks;
	private int currentWeek;
	private int currentYear;

	
	
}
