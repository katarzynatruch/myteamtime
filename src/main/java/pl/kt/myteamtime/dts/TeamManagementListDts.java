package pl.kt.myteamtime.dts;

import java.util.List;

import lombok.Data;

@Data
public class TeamManagementListDts {

	private List<EmployeeTeamLoadDts> employeeTeamLoadDts;

}
