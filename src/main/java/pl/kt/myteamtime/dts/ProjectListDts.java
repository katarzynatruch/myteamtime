package pl.kt.myteamtime.dts;

import java.util.List;

import lombok.Data;
import pl.kt.myteamtime.dto.ProjectDTO;

@Data
public class ProjectListDts {

	List <ProjectDTO> projects;
}
