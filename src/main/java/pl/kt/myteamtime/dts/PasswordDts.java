package pl.kt.myteamtime.dts;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import lombok.Data;
import pl.kt.myteamtime.security.PasswordMatches;
import pl.kt.myteamtime.security.ValidPassword;

@Data
public class PasswordDts {

	@ValidPassword
	private String password;

	@NotNull
	@Size(min = 1)
	private String matchingPassword;

}
