package pl.kt.myteamtime.dts;

import lombok.Data;
import pl.kt.myteamtime.dto.EmployeeDTO;
import pl.kt.myteamtime.dto.ReminderMessageDTO;

import java.util.List;

@Data
public class DefaultReminderMessageDts {

	private String managerIdentificator;
	private ReminderMessageDTO reminderMessageDTO;

	
	
}
