package pl.kt.myteamtime.dts;

import lombok.Data;

import java.util.List;

@Data
public class StatisticsComboDiagramRetrieveDts {

	private List<String> employeeIdentificators;
	private List<String> projectNumbers;
	private int weekFrom;
	private int weekTo;
	private int yearFrom;
	private int yearTo;

}
