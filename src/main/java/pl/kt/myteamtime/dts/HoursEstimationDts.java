package pl.kt.myteamtime.dts;

import java.util.List;

import lombok.Data;
import pl.kt.myteamtime.ds.HoursConsumptionPerWeekDs;

@Data
public class HoursEstimationDts {

	private List<HoursConsumptionPerWeekDs> listOfHoursConsumptionPerWeekDs;
	private int currentWeek;
	private int currentYear;
	
}
