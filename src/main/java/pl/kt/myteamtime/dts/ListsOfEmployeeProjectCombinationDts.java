package pl.kt.myteamtime.dts;

import java.util.List;

import lombok.Data;
import pl.kt.myteamtime.ds.EmployeeWithProjectListDs;
import pl.kt.myteamtime.ds.ProjectWithEmployeeListDs;

@Data
public class ListsOfEmployeeProjectCombinationDts {

	List<EmployeeWithProjectListDs> employeeWithProjectListDs;
	List<ProjectWithEmployeeListDs> projectWithEmployeeListDs;
	
}
