package pl.kt.myteamtime.dts;

import java.util.List;

import lombok.Data;
import pl.kt.myteamtime.ds.WorkLogWeekDs;
import pl.kt.myteamtime.dto.EmployeeDTO;

@Data
public class ApproveWorkLogsByEmployeeDts {

	private EmployeeDTO employee;
	private List<WorkLogWeekDs>  workLogDs;
	
	
}
