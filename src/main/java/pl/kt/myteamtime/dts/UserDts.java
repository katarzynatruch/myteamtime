package pl.kt.myteamtime.dts;

import java.util.List;

import lombok.Data;
import pl.kt.myteamtime.dto.UserDTO;

@Data
public class UserDts {

	private List<UserDTO> users;
}
