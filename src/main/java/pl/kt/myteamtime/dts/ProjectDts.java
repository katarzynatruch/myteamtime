package pl.kt.myteamtime.dts;

import java.util.List;

import lombok.Data;
import pl.kt.myteamtime.entity.Deadline;
import pl.kt.myteamtime.enums.ProjectType;

@Data
public class ProjectDts {

	private Long id;
	private String name;
	private String number;
	private String costCenter;
	private Long maxHours;
	private ProjectType type;

	// Relations

	private List<Deadline> deadlines;
}