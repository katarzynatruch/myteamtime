package pl.kt.myteamtime.dts;

import java.util.List;

import lombok.Data;
import pl.kt.myteamtime.dto.EmployeeDTO;
import pl.kt.myteamtime.dto.TeamDTO;

@Data
public class EmployeeTeamLoadDts {
	private TeamDTO team;
	private List<EmployeeDTO> employees;
	private Long managerId;

}
