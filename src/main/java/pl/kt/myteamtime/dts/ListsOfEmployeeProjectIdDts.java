package pl.kt.myteamtime.dts;

import java.util.List;

import lombok.Data;
import pl.kt.myteamtime.ds.EmployeeIdList;
import pl.kt.myteamtime.ds.EmployeeWithProjectAndLimitsListDs;
import pl.kt.myteamtime.ds.ProjectWithEmployeeListDs;

@Data
public class ListsOfEmployeeProjectIdDts {

	List<EmployeeIdList> employeeIdList;

}
