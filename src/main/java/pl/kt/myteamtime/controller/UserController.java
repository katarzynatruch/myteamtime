package pl.kt.myteamtime.controller;

import java.util.Calendar;
import java.util.Locale;
import java.util.Objects;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.context.MessageSource;
import org.springframework.core.env.Environment;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.context.request.WebRequest;

import pl.kt.myteamtime.controller.util.GenericResponse;
import pl.kt.myteamtime.dto.PasswordDTO;
import pl.kt.myteamtime.dts.PasswordDts;
import pl.kt.myteamtime.entity.User;
import pl.kt.myteamtime.entity.VerificationToken;
import pl.kt.myteamtime.repository.TokenRepository;
import pl.kt.myteamtime.service.SecurityService;
import pl.kt.myteamtime.service.UserService;
import pl.kt.myteamtime.validation.InvalidOldPasswordException;
import pl.kt.myteamtime.validation.UserNotFoundException;

@RestController
@RequestMapping("/my")
public class UserController {

	private final Logger LOGGER = LoggerFactory.getLogger(getClass());
	@Autowired
	private TokenRepository tokenRepository;
	@Autowired
	private Environment env;
	@Autowired
	private SecurityService securityService;
	@Autowired
	private UserService userService;
	@Autowired
	private MessageSource messages;
	@Autowired
	private JavaMailSender mailSender;
	@Autowired
	private ApplicationEventPublisher eventPublisher;
	@Autowired
	private AuthenticationManager authenticationManager;

	@GetMapping("/registrationConfirm")
	public String confirmRegistration(@RequestBody @Valid PasswordDts passwordDts, WebRequest request,
			@RequestParam("token") String token, Locale locale) {

		VerificationToken verificationToken = userService.getVerificationToken(token);
		if (verificationToken == null) {

			return messages.getMessage("auth.message.invalidToken", null, locale);
		}

		User user = verificationToken.getUser();
		Calendar cal = Calendar.getInstance();
		if ((verificationToken.getExpiryDate().getTime() - cal.getTime().getTime()) <= 0) {
			return messages.getMessage("auth.message.expired", null, locale);
		}

		user.setEnabled(true);
		user.setPassword(passwordDts.getPassword());
		userService.saveRegisteredUser(user);
		return messages.getMessage("auth.message.successful", null, locale);
	}

	@PostMapping("/user/resetPassword")
	@ResponseBody
	public GenericResponse resetPassword(HttpServletRequest request, @RequestParam("email") String userEmail) {
		User user = userService.findUserByEmail(userEmail);
		if (user == null) {
			throw new UserNotFoundException();
		}
		String token = UUID.randomUUID().toString();
		userService.createPasswordResetTokenForUser(user, token);
		mailSender.send(constructResetTokenEmail(getAppUrl(request), request.getLocale(), token, user));
		return new GenericResponse(messages.getMessage("message.resetPasswordEmail", null, request.getLocale()));
	}

	@GetMapping("/user/changePassword")
	public String showChangePasswordPage(Locale locale, @RequestParam("id") long id,
			@RequestParam("token") String token) {
		String result = securityService.validatePasswordResetToken(id, token);
		if (result != null) {
			return "redirect:/login?lang=" + locale.getLanguage();
		}
		return "redirect:/updatePassword.html?lang=" + locale.getLanguage();
	}

	@PostMapping("/user/updatePassword")
	@PreAuthorize("hasRole('READ_PRIVILEGE')")
	@ResponseBody
	public GenericResponse changeUserPassword(Locale locale, @RequestParam("password") String password,
			@RequestParam("oldPassword") String oldPassword) {
		User user = userService.findUserByEmail(SecurityContextHolder.getContext().getAuthentication().getName());

		if (!userService.checkIfValidOldPassword(user, oldPassword)) {
			throw new InvalidOldPasswordException();
		}
		userService.changeUserPassword(user, password);
		return new GenericResponse(messages.getMessage("message.updatePasswordSuc", null, locale));
	}

	@PostMapping("/user/savePassword")
	@ResponseBody
	public GenericResponse savePassword(Locale locale, @Valid PasswordDTO passwordDTO) {
		User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();

		userService.changeUserPassword(user, passwordDTO.getNewPassword());
		return new GenericResponse(messages.getMessage("message.resetPasswordSuc", null, locale));
	}

	private SimpleMailMessage constructResetTokenEmail(String contextPath, Locale locale, String token, User user) {
		String url = contextPath + "/user/changePassword?id=" + user.getId() + "&token=" + token;
		String message = messages.getMessage("message.resetPassword", null, locale);
		return constructEmail("Reset Password", message + " \r\n" + url, user);
	}

	private SimpleMailMessage constructEmail(String subject, String body, User user) {
		SimpleMailMessage email = new SimpleMailMessage();
		email.setSubject(subject);
		email.setText(body);
		email.setTo(user.getEmail());
		email.setFrom(Objects.requireNonNull(env.getProperty("support.email")));
		return email;
	}

	private String getAppUrl(HttpServletRequest request) {
		return "http://" + request.getServerName() + ":" + request.getServerPort() + request.getContextPath();
	}
}
