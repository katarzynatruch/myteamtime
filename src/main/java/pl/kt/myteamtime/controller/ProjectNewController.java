package pl.kt.myteamtime.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import pl.kt.myteamtime.dts.ProjectDts;
import pl.kt.myteamtime.facade.ProjectManagementFacade;

//DLG_Project_New
@RestController
@RequestMapping("/new-project")
public class ProjectNewController {

	@Autowired
	private ProjectManagementFacade projectManagementFacade;

	@PostMapping("/add")
	public void saveOrUpdateProject(@RequestBody ProjectDts projectDts) {

		projectManagementFacade.saveOrUpdateProject(projectDts);

	}

}
