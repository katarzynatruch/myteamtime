package pl.kt.myteamtime.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import pl.kt.myteamtime.dto.UserDTO;
import pl.kt.myteamtime.dts.UserDts;
import pl.kt.myteamtime.entity.User;
import pl.kt.myteamtime.facade.AdminFacade;
import pl.kt.myteamtime.service.UserService;
import pl.kt.myteamtime.validation.EmailExistsException;
import pl.kt.myteamtime.validation.UserAlreadyExistException;

//DLG_Admin
@RestController
@RequestMapping("/admin")
public class AdminController {

	@Autowired
	AdminFacade adminFacade;

	@Autowired
    UserService userService;

	@GetMapping("/loadUsers")
	public UserDts loadView() {

		return adminFacade.findAllUsers();
	}

	@PostMapping("/addUser")
	public ModelAndView addUser(@Valid @ModelAttribute("user") UserDTO accountDto, BindingResult result,
			ModelMap model) {

        ModelAndView mav = new ModelAndView();
        if (result.hasErrors()) {
            throw new UserAlreadyExistException();
        }
        User registered = createUserAccount(accountDto);


		mav.setViewName("forward:homepage.html");

		return mav;
	}
    private User createUserAccount(UserDTO accountDto) {
        User registered = null;
        try {
            registered = userService.registerNewUserAccount(accountDto);
        } catch (EmailExistsException e) {
            return null;
        }
        return registered;
    }
}
