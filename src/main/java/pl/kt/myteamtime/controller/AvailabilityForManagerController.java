package pl.kt.myteamtime.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import pl.kt.myteamtime.ds.AvailabilitySingleEmployeeDs;
import pl.kt.myteamtime.ds.AvailabilitySingleEmployeeUpdateDs;
import pl.kt.myteamtime.dts.AvailabilityForManagerDts;
import pl.kt.myteamtime.facade.AvailabilityFacade;

//DLG_Availability
@RestController
@RequestMapping("/availability")
public class AvailabilityForManagerController {

	@Autowired
	private
	AvailabilityFacade availabilityFacade;
	
	@GetMapping("/{managerIdentificator}")
	public AvailabilityForManagerDts loadLogs(@PathVariable String managerIdentificator) {
		
		return availabilityFacade.loadLogs(managerIdentificator);
	}
	
	@PostMapping("/")
	public void saveLogs(@RequestBody AvailabilitySingleEmployeeUpdateDs availabilitySingleEmployeeUpdateDs) {
		
		availabilityFacade.saveLogs(availabilitySingleEmployeeUpdateDs);
	}
	
}
