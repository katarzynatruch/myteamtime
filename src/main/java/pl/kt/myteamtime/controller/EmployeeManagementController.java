package pl.kt.myteamtime.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import pl.kt.myteamtime.dto.UserDTO;
import pl.kt.myteamtime.dts.EmployeesDts;
import pl.kt.myteamtime.dts.UserDts;
import pl.kt.myteamtime.entity.User;
import pl.kt.myteamtime.facade.EmployeeManagementFacade;

import javax.validation.Valid;
import java.util.List;

//DLG_Employee_Management
@RestController
@RequestMapping("/employee")
public class EmployeeManagementController {

	@Autowired
	private
	EmployeeManagementFacade employeeManagementFacade;
	
	@GetMapping(value = "/elist/{managerIdentificator}")
	public EmployeesDts loadView(@PathVariable String managerIdentificator) {
		
		return employeeManagementFacade.loadView(managerIdentificator);
		
	}

	//unused, go to UserManagementController
	@PostMapping(value = "/elist/")
	public void saveOrUpdateEmployee(@RequestBody EmployeesDts employeesDts) {
		
		employeeManagementFacade.saveOrUpdateEmployee(employeesDts);
		
	}

	@PostMapping("/delete/{employeeId}")
	public void deleteEmployee(@PathVariable Long employeeId){

		employeeManagementFacade.deleteEmployee(employeeId);

	}


}
