package pl.kt.myteamtime.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import pl.kt.myteamtime.dts.EmployeesReminderDts;

//DLG_Login_Page
//@CrossOrigin(origins = "http://localhost:3000")
@RestController
public class PerformLoginController {

    @PostMapping("/perform_login")
    public void sendLoginRequest() {

        System.out.println("perform-login");

    }

    @GetMapping("/perform_login")
    public String login() {
        return "perform-login";
    }

}
