package pl.kt.myteamtime.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import pl.kt.myteamtime.dts.ListsOfEmployeeProjectCombinationDts;
import pl.kt.myteamtime.dts.ListsOfEmployeeProjectIdDts;
import pl.kt.myteamtime.dts.ProjectManagementListDts;
import pl.kt.myteamtime.facade.ProjectManagementFacade;

//DLG_Project_Management
@RestController
@RequestMapping("/project-management")
public class ProjectManagementController {

	@Autowired
	private
	ProjectManagementFacade projectManagementFacade;

	@GetMapping("/general-list/{managerIdentificator}")
	public ProjectManagementListDts loadView(@PathVariable String managerIdentificator) {

		return projectManagementFacade.loadProjectAndEmployeeData(managerIdentificator);

	}

	@GetMapping("/list/{managerIdentificator}")
	public ListsOfEmployeeProjectCombinationDts assignedProjectAndEmployee(@PathVariable String managerIdentificator) {

		return projectManagementFacade.loadEmployeeProjectCombinatedLists(managerIdentificator);
	}

	@PostMapping("/list/")
	public void saveOrUpdateAssignedProjects(@RequestBody ListsOfEmployeeProjectIdDts listsOfEmployeeProjectIdDts) {

		projectManagementFacade.saveOrUpdateAssignedProjectsToEmployee(listsOfEmployeeProjectIdDts);

	}

	@PostMapping("/delete")
	public  void  deleteProject(@PathVariable Long projectId){

		projectManagementFacade.delete(projectId);
	}

}
