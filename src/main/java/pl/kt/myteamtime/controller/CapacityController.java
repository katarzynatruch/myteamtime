package pl.kt.myteamtime.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import pl.kt.myteamtime.dts.CapacityDts;
import pl.kt.myteamtime.dts.CapacityRetrieveDts;
import pl.kt.myteamtime.dts.GraphFilterListDts;
import pl.kt.myteamtime.facade.CapacityFacade;

//DLG_Capacity
@RestController
@RequestMapping("/statistics")
public class CapacityController {

	
	@Autowired
	private
	CapacityFacade capacityFacade;	
	
	
	@GetMapping("/capacityGraph")
	public CapacityDts loadGraph(@RequestBody CapacityRetrieveDts capacityRetrieveDts) {
		
		return capacityFacade.loadGraph(capacityRetrieveDts);
		
	}
	
	@GetMapping("/filters/{managerIdentificator}")
	public GraphFilterListDts loadFilters(@PathVariable String managerIdentificator) {
		
		return capacityFacade.loadFilters(managerIdentificator);
	}

	@PostMapping("/export-to-excel-capacity")
	public void exportToExcel(@RequestBody CapacityRetrieveDts capacityRetrieveDts){
		CapacityDts capacityDts = capacityFacade.loadGraph(capacityRetrieveDts);
		capacityFacade.exportToExcel(capacityDts);
	}
	
}
