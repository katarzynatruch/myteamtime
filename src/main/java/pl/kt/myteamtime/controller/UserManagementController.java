package pl.kt.myteamtime.controller;

import java.util.Locale;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.context.MessageSource;
import org.springframework.core.env.Environment;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.context.request.WebRequest;

import pl.kt.myteamtime.controller.util.GenericResponse;
import pl.kt.myteamtime.dto.UserDTO;
import pl.kt.myteamtime.dts.EmployeesDts;
import pl.kt.myteamtime.entity.User;
import pl.kt.myteamtime.entity.VerificationToken;
import pl.kt.myteamtime.facade.EmployeeManagementFacade;
import pl.kt.myteamtime.registration.OnRegistrationCompleteEvent;
import pl.kt.myteamtime.service.UserService;
import pl.kt.myteamtime.validation.EmailExistsException;
import pl.kt.myteamtime.validation.UserAlreadyExistException;

@RestController
@RequestMapping("/users")
public class UserManagementController {

	private final Logger LOGGER = LoggerFactory.getLogger(getClass());
	private final Logger logger = LoggerFactory.getLogger(getClass());
	@Autowired
	Environment env;
	@Autowired
	private UserService userService;
	@Autowired
	private MessageSource messages;
	@Autowired
	private JavaMailSender mailSender;
	@Autowired
	private ApplicationEventPublisher eventPublisher;
	@Autowired
	private AuthenticationManager authenticationManager;
	@Autowired
	private
	EmployeeManagementFacade employeeManagementFacade;

	@GetMapping(value = "/elist/{managerIdentificator}")
	public EmployeesDts loadView(@PathVariable String managerIdentificator) {

		return employeeManagementFacade.loadView(managerIdentificator);

	}

	//unused, go to registerUserAccount
	@PostMapping(value = "/elist/")
	public void saveOrUpdateEmployee(@RequestBody EmployeesDts employeesDts) {

		employeeManagementFacade.saveOrUpdateEmployee(employeesDts);

	}

	@PostMapping("/delete/{employeeId}")
	public void deleteEmployee(@PathVariable Long employeeId){

		employeeManagementFacade.deleteEmployee(employeeId);

	}

	@PostMapping("/user/registration")
	public GenericResponse registerUserAccount(@RequestBody @Valid UserDTO accountDto, HttpServletRequest request) {
		logger.debug("Registering value = user account with information: {}", accountDto);
		User registered = createUserAccount(accountDto);
		if (registered == null) {
			throw new UserAlreadyExistException();
		}
	//	String appUrl = "http://" + request.getServerName() + ":" + request.getServerPort() + request.getContextPath();

	//	eventPublisher.publishEvent(new OnRegistrationCompleteEvent(registered, request.getLocale(), appUrl));

		return new GenericResponse("success");
	}

	@PostMapping("/giveAccess")
	public void giveAccess(@RequestParam String userIdentificator,  WebRequest request
			) {
	//,	BindingResult result, Errors errors
		User registered = userService.findByIdentificator(userIdentificator);
		try {
			String appUrl = request.getContextPath();
			eventPublisher.publishEvent(new OnRegistrationCompleteEvent(registered, request.getLocale(), appUrl));
		} catch (Exception me) {
		}
	}

	private User createUserAccount(UserDTO accountDto) {
		User registered = null;
		try {
			registered = userService.registerNewUserAccount(accountDto);
		} catch (EmailExistsException e) {
			return null;
		}
		return registered;
	}

	@GetMapping("/user/resendRegistrationToken")
	public GenericResponse resendRegistrationToken(HttpServletRequest request,
			@RequestParam("token") String existingToken) {
		VerificationToken newToken = userService.generateNewVerificationToken(existingToken);

		User user = userService.getUser(newToken.getToken());
		String appUrl = "http://" + request.getServerName() + ":" + request.getServerPort() + request.getContextPath();
		SimpleMailMessage email = constructResendVerificationTokenEmail(appUrl, request.getLocale(), newToken, user);
		mailSender.send(email);

		return new GenericResponse(messages.getMessage("message.resendToken", null, request.getLocale()));
	}

	private SimpleMailMessage constructResendVerificationTokenEmail(String contextPath, Locale locale,
			VerificationToken newToken, User user) {
		String confirmationUrl = contextPath + "/regitrationConfirm.html?token=" + newToken.getToken();
		String message = messages.getMessage("message.resendToken", null, locale);
		SimpleMailMessage email = new SimpleMailMessage();
		email.setSubject("Resend Registration Token");
		email.setText(message + " rn" + confirmationUrl);
		email.setFrom(env.getProperty("support.email"));
		email.setTo(user.getEmail());
		return email;
	}
}
