package pl.kt.myteamtime.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import pl.kt.myteamtime.ds.EmployeeTeamUpdateDs;
import pl.kt.myteamtime.dts.EmployeeTeamUpdateDts;
import pl.kt.myteamtime.dts.TeamManagementListDts;
import pl.kt.myteamtime.facade.TeamManagementFacade;
import pl.kt.myteamtime.service.TeamService;

import java.util.List;

//DLG_Team_Management
@RestController
@RequestMapping("/team")
public class TeamManagementController {

	@Autowired
	private
	TeamManagementFacade teamManagementFacade;

	@Autowired
	TeamService teamService;

	@GetMapping("/management/{managerIdentificator}")
	public TeamManagementListDts loadView(@PathVariable String managerIdentificator) {

		return teamManagementFacade.loadTabData(managerIdentificator);

	}


	@PostMapping("/manageTeams")
	public void saveOrUpdateTeams(@RequestBody EmployeeTeamUpdateDts employeeTeamUpdateDts) {

		teamManagementFacade.saveOrUpdateData(employeeTeamUpdateDts);

	}
}
