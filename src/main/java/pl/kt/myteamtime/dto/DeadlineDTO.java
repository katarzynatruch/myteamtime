package pl.kt.myteamtime.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.Data;
import pl.kt.myteamtime.entity.Project;
import pl.kt.myteamtime.enums.ProjectMilestone;

@Data
public class DeadlineDTO {

	
	private Long id;
	private ProjectMilestone milestones;
	private Integer week;
	private Integer year;
	private String comment;
	
	// Relations
	@JsonIgnore
	private Project project;
}
