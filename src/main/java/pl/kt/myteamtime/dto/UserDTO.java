package pl.kt.myteamtime.dto;

import lombok.Data;
import pl.kt.myteamtime.entity.Role;
import pl.kt.myteamtime.security.PasswordMatches;
import pl.kt.myteamtime.security.ValidPassword;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@Data
@PasswordMatches
public class UserDTO {

    @NotNull
    @NotEmpty
    private String firstName;

    @NotNull
    @NotEmpty
    private String lastName;

 //   @ValidPassword
    private String password;
    private String matchingPassword;

    @NotNull
    @NotEmpty
    private String identificator;

    @NotNull
    @NotEmpty
    private String email;

    @NotNull
    @NotEmpty
    private String role1;

    private String role2;

    private String role3;

    private String role4;

    private String role5;

    private boolean enabled;
    private boolean active;
}
