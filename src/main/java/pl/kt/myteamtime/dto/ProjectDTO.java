package pl.kt.myteamtime.dto;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.Data;
import pl.kt.myteamtime.entity.Deadline;
import pl.kt.myteamtime.entity.Employee;
import pl.kt.myteamtime.entity.ProjectEmployeeMaxHours;
import pl.kt.myteamtime.entity.WorkLog;
import pl.kt.myteamtime.enums.ProjectType;

@Data
public class ProjectDTO {

	private Long id;
	private String name;
	private String number;
	private String costCenter;
	private Long maxHours;
	private ProjectType type;
	private Boolean isActive;

	// Relations

	@JsonIgnore
	private List<WorkLog> workLogs;
	@JsonIgnore
	private List<Deadline> deadlines;
	@JsonIgnore
	private List<Employee> employees;
	@JsonIgnore
	private List<ProjectEmployeeMaxHours> listOfProjectEmployeeMaxHours;
}