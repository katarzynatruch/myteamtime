package pl.kt.myteamtime.dto;

import lombok.Data;

@Data
public class ProjectEmployeeMaxHoursDTO {

	private Long id;
	private Integer maxHours;
	ProjectDTO project;
	EmployeeDTO employee;
}
