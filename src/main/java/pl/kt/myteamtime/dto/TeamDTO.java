package pl.kt.myteamtime.dto;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.Data;
import pl.kt.myteamtime.entity.Manager;
import pl.kt.myteamtime.entity.WorkLog;

@Data
public class TeamDTO {

	private Long id;
	private String name;

	// Relations

//	@JsonIgnore
//	private List<WorkLog> workLogs;
	@JsonIgnore
	private Manager manager;
	@JsonIgnore
	private List<EmployeeDTO> employees;

}