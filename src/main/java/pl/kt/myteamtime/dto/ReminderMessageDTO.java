package pl.kt.myteamtime.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import pl.kt.myteamtime.entity.Manager;

@Data
public class ReminderMessageDTO {

    private Long id;
    private String defaultReminderMessage;
    @JsonIgnore
    private Manager manager;
}
