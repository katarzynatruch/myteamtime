package pl.kt.myteamtime.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.Data;
import pl.kt.myteamtime.entity.Employee;
import pl.kt.myteamtime.enums.Event;

@Data
public class AvailabilityDTO {

	private Event event;
	private Integer day;
	private Integer week;
	private Integer year;
	private Integer month;
	@JsonIgnore
	private Employee employee;
	
}
