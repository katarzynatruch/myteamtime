package pl.kt.myteamtime.dto;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.Data;
import pl.kt.myteamtime.entity.Availability;
import pl.kt.myteamtime.entity.ProjectEmployeeMaxHours;
import pl.kt.myteamtime.security.ValidEmail;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@Data
public class EmployeeDTO {

	private Long id;
	@NotNull
	@NotEmpty
	private String firstName;
	@NotNull
	@NotEmpty
	private String lastName;
	@NotNull
	@NotEmpty
	@ValidEmail
	private String email;
	@NotNull
	@NotEmpty
	private String identificator;
	private Boolean isActive;

	// Relations

	// @JsonProperty(access = Access.READ_ONLY)
	@JsonIgnore
	private List<TeamDTO> teams;
	@JsonIgnore
	private List<ProjectDTO> projects;
	@JsonIgnore
	private List<WorkLogDTO> workLogs;
	@JsonIgnore
	private List<ProjectEmployeeMaxHours> listOfProjectEmployeeMaxHours;
	@JsonIgnore
	private List<Availability> availabilityList;

}