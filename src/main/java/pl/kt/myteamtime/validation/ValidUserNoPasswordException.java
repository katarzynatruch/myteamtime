package pl.kt.myteamtime.validation;

public final class ValidUserNoPasswordException extends RuntimeException {

    private static final long serialVersionUID = 5861310537386287163L;

    public ValidUserNoPasswordException() {
        super();
    }

    public ValidUserNoPasswordException(final String message, final Throwable cause) {
        super(message, cause);
    }

    public ValidUserNoPasswordException(final String message) {
        super(message);
    }

    public ValidUserNoPasswordException(final Throwable cause) {
        super(cause);
    }

}
