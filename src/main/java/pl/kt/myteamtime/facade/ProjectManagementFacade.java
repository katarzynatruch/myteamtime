package pl.kt.myteamtime.facade;

import pl.kt.myteamtime.dts.*;

public interface ProjectManagementFacade {

	ProjectListDts loadTabData();

	void saveOrUpdateProject(ProjectDts projectDts);

	ProjectManagementListDts loadProjectAndEmployeeData(String managerIdentificator);

	ListsOfEmployeeProjectCombinationDts loadEmployeeProjectCombinatedLists(String managerIdentificator);

	void saveOrUpdateAssignedProjectsToEmployee(ListsOfEmployeeProjectIdDts listsOfEmployeeProjectIdDts);

    void delete(Long projectId);
}
