package pl.kt.myteamtime.facade;

import pl.kt.myteamtime.ds.AvailabilitySingleEmployeeUpdateDs;
import pl.kt.myteamtime.dts.AvailabilityForManagerDts;

public interface AvailabilityFacade {

	AvailabilityForManagerDts loadLogs(String managerIdentificator);

	void saveLogs(AvailabilitySingleEmployeeUpdateDs availabilitySingleEmployeeDs);

}
