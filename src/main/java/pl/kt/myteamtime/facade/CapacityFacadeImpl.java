package pl.kt.myteamtime.facade;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import pl.kt.myteamtime.ds.CapacityOfEmployeeDs;
import pl.kt.myteamtime.ds.CapacityParametersDs;
import pl.kt.myteamtime.dto.EmployeeDTO;
import pl.kt.myteamtime.dto.ProjectDTO;
import pl.kt.myteamtime.dto.TeamDTO;
import pl.kt.myteamtime.dts.*;
import pl.kt.myteamtime.enums.ProjectType;
import pl.kt.myteamtime.service.*;

@Service
public class CapacityFacadeImpl implements CapacityFacade {

	@Autowired
	private
	WorkLogService workLogService;

	@Autowired
	private
	DataAndTimeService dataAndTime;
	
	@Autowired
	private
	TeamService teamService;
	
	@Autowired
	private
	EmployeeService employeeService;
	
	@Autowired
	private
	ProjectService projectService;

	@Autowired
	private ExcelService excelService;
	

	@Override
	public CapacityDts loadGraph(CapacityRetrieveDts capacityRetrieveDts) {

		CapacityDts capacityDts = new CapacityDts();
		List<CapacityOfEmployeeDs> capacityOfEmployeeDsList = new ArrayList<CapacityOfEmployeeDs>();
		capacityDts.setCapacityOfEmployees(capacityOfEmployeeDsList);

		for (String employeeIdentificator : capacityRetrieveDts.getEmployeeIdentificators()) {

			int week = dataAndTime.getCurrentWeekGlobal();
			int year = dataAndTime.getCurrenYearGlobal();

			CapacityOfEmployeeDs capacityOfEmployeeDs = new CapacityOfEmployeeDs();
			capacityOfEmployeeDs.setEmployee(employeeService.findByEmployeeIdentificatorDTO(employeeIdentificator));
			List<CapacityParametersDs> capacityParametersDsList = new ArrayList<CapacityParametersDs>();
			

			for (int i = 0; i < 12; i++) {
				
				CapacityParametersDs capacityParametersDs = new CapacityParametersDs();

				List<ProjectDTO> projects = new ArrayList<>();
				for(String projectNumber : capacityRetrieveDts.getProjectNumbers()){
					projects.add(projectService.findProjectByProjectNumberDTO(projectNumber));
				}
				
				capacityParametersDs.setWeek(week);
				capacityParametersDs.setYear(year);
				long calculatedHoursForEmployee = workLogService.calculateHoursForEmployee(employeeService.findByEmployeeIdentificatorDTO(employeeIdentificator),
						projects, week, year);
				capacityParametersDs.setWorkedHours(calculatedHoursForEmployee);

				
				
				
				if (week < dataAndTime.getMaxWeekOfTheYear(year)) {
					week++;
				} else {

					year++;
					week = 1;
				}
				
				capacityParametersDsList.add(capacityParametersDs);
			}
			
			capacityOfEmployeeDs.setCapacityParameters(capacityParametersDsList);
			capacityOfEmployeeDsList.add(capacityOfEmployeeDs);
		}

		return capacityDts;
	}

	@Override
	public GraphFilterListDts loadFilters(String managerIdentificator) {

		GraphFilterListDts graphFilterListDts = new GraphFilterListDts();
		graphFilterListDts.setCommonProjects(projectService.findByType(ProjectType.CM));
		graphFilterListDts.setRegularProjects(projectService.findByType(ProjectType.RG));
		
		List<TeamDTO> teams = teamService.findTeamsByManagerIdentificator(managerIdentificator);
		List<EmployeeTeamLoadDts> employeesInTeams = new ArrayList<>();
		for (TeamDTO team : teams) {

			EmployeeTeamLoadDts employeesInTeam = new EmployeeTeamLoadDts();
			List<EmployeeDTO> employees = employeeService.findEmployeesByTeamId(team.getId());
			employeesInTeam.setTeam(team);
			employeesInTeam.setEmployees(employees);
			employeesInTeams.add(employeesInTeam);
			
		}
		graphFilterListDts.setEmployeesInTeams(employeesInTeams);
		return graphFilterListDts;
	}

    @Override
    public void exportToExcel(CapacityDts capacityDts) {
        excelService.prepareCapacityGraph(capacityDts.getCapacityOfEmployees());
    }

}
