package pl.kt.myteamtime.facade;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import pl.kt.myteamtime.ds.AvailabilitySingleEmployeeDs;
import pl.kt.myteamtime.ds.AvailabilitySingleEmployeeUpdateDs;
import pl.kt.myteamtime.dto.AvailabilityDTO;
import pl.kt.myteamtime.dto.EmployeeDTO;
import pl.kt.myteamtime.dts.AvailabilityForManagerDts;
import pl.kt.myteamtime.service.AvailabilityService;
import pl.kt.myteamtime.service.EmployeeService;

@Service
public class AvailabilityFacadeImpl implements AvailabilityFacade {

	@Autowired
	EmployeeService employeeService;
	
	@Autowired
	AvailabilityService availabilityService;
	
	@Override
	public AvailabilityForManagerDts loadLogs(String managerIdentificator) {

		AvailabilityForManagerDts availabilityForManagerDts = new AvailabilityForManagerDts();
		List<AvailabilitySingleEmployeeDs> availabilitySingleEmployeeDsList = new ArrayList<AvailabilitySingleEmployeeDs>();
		List<EmployeeDTO> employees = employeeService.findEmployeeByManagerIdentificator(managerIdentificator);
		
		for (EmployeeDTO employee : employees) {

			AvailabilitySingleEmployeeDs availabilitySingleEmployeeDs = new AvailabilitySingleEmployeeDs();
			availabilitySingleEmployeeDs.setEmployee(employee);
			List<AvailabilityDTO> availabilityList = availabilityService.findByEmployee(employee);
			availabilitySingleEmployeeDs.setAvailability(availabilityList);
		
			availabilitySingleEmployeeDsList.add(availabilitySingleEmployeeDs);
		}			
		availabilityForManagerDts.setAvailabilitySingleEmployeeDsList(availabilitySingleEmployeeDsList);
		return availabilityForManagerDts;
	}

	@Override
	public void saveLogs(AvailabilitySingleEmployeeUpdateDs availabilitySingleEmployeeUpdateDs) {

		availabilityService.save(availabilitySingleEmployeeUpdateDs);
		
	}

}
