package pl.kt.myteamtime.facade;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import pl.kt.myteamtime.ds.EmployeeIdList;
import pl.kt.myteamtime.ds.EmployeeWithProjectListDs;
import pl.kt.myteamtime.ds.ProjectWithEmployeeListDs;
import pl.kt.myteamtime.dto.EmployeeDTO;
import pl.kt.myteamtime.dto.ProjectDTO;
import pl.kt.myteamtime.dts.*;
import pl.kt.myteamtime.service.EmployeeService;
import pl.kt.myteamtime.service.ProjectService;

@Service
public class ProjectManagementFacadeImpl implements ProjectManagementFacade {

	@Autowired
	ProjectService projectService;

	@Autowired
	EmployeeService employeeService;

	@Override
	public ProjectListDts loadTabData() {

		return projectService.loadTabData();
	}

	@Override
	public void saveOrUpdateProject(ProjectDts projectDts) {

		ProjectDTO projectDTO = new ProjectDTO();

		if (projectDts.getCostCenter() != null)
			projectDTO.setCostCenter(projectDts.getCostCenter());
		if (projectDts.getDeadlines() != null)
			projectDTO.setDeadlines(projectDts.getDeadlines());
		if (projectDts.getId() != null)
			projectDTO.setId(projectDts.getId());
		if (projectDts.getMaxHours() != null)
			projectDTO.setMaxHours(projectDts.getMaxHours());
		if (projectDts.getName() != null)
			projectDTO.setName(projectDts.getName());
		if (projectDts.getNumber() != null)
			projectDTO.setNumber(projectDts.getNumber());
		if (projectDts.getType() != null)
			projectDTO.setType(projectDts.getType());

		projectService.saveOrUpdate(projectDTO);

	}

	@Override
	public ProjectManagementListDts loadProjectAndEmployeeData(String managerIdentificator) {

		List<EmployeeDTO> employees = employeeService.findEmployeeByManagerIdentificator(managerIdentificator);
		List<ProjectDTO> projects = projectService.findAll();

		ProjectManagementListDts projectManagementListDts = new ProjectManagementListDts();
		projectManagementListDts.setEmployees(employees);
		projectManagementListDts.setProjects(projects);

		return projectManagementListDts;
	}

	@Override
	public ListsOfEmployeeProjectCombinationDts loadEmployeeProjectCombinatedLists(String managerIdentificator) {

		List<ProjectWithEmployeeListDs> projectsAndItsEmployees = projectService
				.findProjectWithEmployees(managerIdentificator);

		List<EmployeeWithProjectListDs> employeesAndTheirProjects = employeeService
				.findEmployeeWithProjects(managerIdentificator);

		ListsOfEmployeeProjectCombinationDts listsOfEmployeeProjectCombinationDts = new ListsOfEmployeeProjectCombinationDts();
		listsOfEmployeeProjectCombinationDts.setEmployeeWithProjectListDs(employeesAndTheirProjects);
		listsOfEmployeeProjectCombinationDts.setProjectWithEmployeeListDs(projectsAndItsEmployees);

		return listsOfEmployeeProjectCombinationDts;
	}

	@Override
	public void saveOrUpdateAssignedProjectsToEmployee(ListsOfEmployeeProjectIdDts listsOfEmployeeProjectIdDts) {

		List<EmployeeIdList> employeeIdList = listsOfEmployeeProjectIdDts.getEmployeeIdList();

		for (EmployeeIdList employeeCombination : employeeIdList) {

			employeeService.saveEmployeeProjectRelations(employeeCombination.getEmployeeId(),
					employeeCombination.getProjectIdWithMaxHoursDsList());
		}
	}

    @Override
    public void delete(Long projectId) {

		projectService.delete(projectId);
    }

}
