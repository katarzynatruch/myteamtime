package pl.kt.myteamtime.facade;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import pl.kt.myteamtime.ds.WorkLogWeekDs;
import pl.kt.myteamtime.dto.WorkLogDTO;
import pl.kt.myteamtime.dts.*;
import pl.kt.myteamtime.service.*;

@Service
public class EmployeeTimeReportFacadeImpl implements EmployeeTimeReportFacade {

	@Autowired
	ReminderMessageService reminderMessageService;
	@Autowired
	private EmployeeService employeeService;
	@Autowired
	private DataAndTimeService dataAndTime;
	@Autowired
	private ProjectService projectService;
	@Autowired
	private WorkLogService workLogService;

	@Override
	public EmployeeTimeReportDts loadTabData(EmployeeTimeReportUpdateDts employeeTimeReportUpdateDts) {

		EmployeeTimeReportDts tabDts = new EmployeeTimeReportDts();
		tabDts.setCurrentWeek(dataAndTime.getCurrentWeekGlobal());
		tabDts.setCurrentYear(dataAndTime.getCurrenYearGlobal());
		tabDts.setEmployee(employeeService.findByEmployeeIdentificatorDTO(employeeTimeReportUpdateDts.getIdentificator()));
		tabDts.setUserProjects(
				projectService.findProjectsByEmployeeIdentificator(employeeTimeReportUpdateDts.getIdentificator()));
		tabDts.setWorkLogWeeks(
				workLogService.findWorkLogWeeksByUserIdentificator(employeeTimeReportUpdateDts.getIdentificator()));

		return tabDts;
	}

	@Override
	public void saveOrUpdate(EmployeeTimeReportSaveDts employeeTimeReportSaveDts) {
		employeeTimeReportSaveDts.getWorkLogWeeks().stream()
				.forEach(w -> w.getWorkLogs().stream().forEach(ww -> ww.setWeek(w.getWeek())));
		employeeTimeReportSaveDts.getWorkLogWeeks().stream()
				.forEach(w -> w.getWorkLogs().stream().forEach(ww -> ww.setYear(w.getYear())));
		List<WorkLogDTO> workLogs = new ArrayList<WorkLogDTO>();
		employeeTimeReportSaveDts.getWorkLogWeeks().stream().forEach(w -> workLogs.addAll(w.getWorkLogs()));

		workLogs.stream().forEach(w -> w.setEmployee(employeeTimeReportSaveDts.getEmployee()));
		workLogService.saveOrUpdate(workLogs);

	}

	@Override
	public void approveWorkLogsByEmployee(ApproveWorkLogsByEmployeeDts approveWorkLogsByEmployeeDts) {

		List<WorkLogWeekDs> workLogWeekDsList = approveWorkLogsByEmployeeDts.getWorkLogDs();
		for (WorkLogWeekDs workLogWeekDs : workLogWeekDsList) {

			if (workLogWeekDs.getYear() == dataAndTime.getCurrenYearGlobal()) {
				if (workLogWeekDs.getWeek() == dataAndTime.getCurrentWeekGlobal()) {

					for (WorkLogDTO workLog : workLogWeekDs.getWorkLogs()) {

						workLog.setIsConfirmedByEmployee(true);
					}
					workLogService.saveOrUpdate(workLogWeekDs.getWorkLogs());
				}
			}
			if (workLogWeekDs.getYear() == (dataAndTime.getCurrenYearGlobal() - 1)) {
				if (workLogWeekDs.getWeek() == dataAndTime.getMaxWeekOfTheYear(dataAndTime.getCurrenYearGlobal() - 1)) {
					for (WorkLogDTO workLog : workLogWeekDs.getWorkLogs()) {

						workLog.setIsConfirmedByEmployee(true);
					}
					workLogService.saveOrUpdate(workLogWeekDs.getWorkLogs());
				}
			}
			if (workLogWeekDs.getYear() == dataAndTime.getCurrenYearGlobal()) {
				if (workLogWeekDs.getWeek() == (dataAndTime.getCurrentWeekGlobal() - 1)) {
					for (WorkLogDTO workLog : workLogWeekDs.getWorkLogs()) {

						workLog.setIsConfirmedByEmployee(true);
					}
					workLogService.saveOrUpdate(workLogWeekDs.getWorkLogs());
				}
			}

		}

	}

	@Override
	public void approveWorkLogsByManager(ApproveWorkLogsByEmployeeDts approveWorkLogsByEmployeeDts) {

		List<WorkLogWeekDs> workLogWeekDsList = approveWorkLogsByEmployeeDts.getWorkLogDs();
		for (WorkLogWeekDs workLogWeekDs : workLogWeekDsList) {

			if (workLogWeekDs.getYear() == dataAndTime.getCurrenYearGlobal()) {
				if (workLogWeekDs.getWeek() == dataAndTime.getCurrentWeekGlobal()) {

					for (WorkLogDTO workLog : workLogWeekDs.getWorkLogs()) {

						workLog.setIsConfirmedByManager(true);
					}
					workLogService.saveOrUpdate(workLogWeekDs.getWorkLogs());
				}
			}
			if (workLogWeekDs.getYear() == (dataAndTime.getCurrenYearGlobal() - 1)) {
				if (workLogWeekDs.getWeek() == dataAndTime.getMaxWeekOfTheYear(dataAndTime.getCurrenYearGlobal() - 1)) {
					for (WorkLogDTO workLog : workLogWeekDs.getWorkLogs()) {

						workLog.setIsConfirmedByManager(true);
					}
					workLogService.saveOrUpdate(workLogWeekDs.getWorkLogs());
				}
			}
			if (workLogWeekDs.getYear() == dataAndTime.getCurrenYearGlobal()) {
				if (workLogWeekDs.getWeek() == (dataAndTime.getCurrentWeekGlobal() - 1)) {
					for (WorkLogDTO workLog : workLogWeekDs.getWorkLogs()) {

						workLog.setIsConfirmedByManager(true);
					}
					workLogService.saveOrUpdate(workLogWeekDs.getWorkLogs());
				}
			}

		}

	}

    @Override
    public void deleteWorkLog(WorkLogDeleteDts workLogDeleteDts) {
        workLogService.deleteWorkLog(workLogDeleteDts.getWorkLogId());
    }

}
