package pl.kt.myteamtime.facade;

import pl.kt.myteamtime.dts.*;

public interface EmployeeTimeReportFacade {

	public EmployeeTimeReportDts loadTabData(EmployeeTimeReportUpdateDts employeeTimeReportUpdateDts);

	public void saveOrUpdate(EmployeeTimeReportSaveDts employeeTimeReportSaveDts);

	public void approveWorkLogsByEmployee(ApproveWorkLogsByEmployeeDts approveWorkLogsByEmployeeDts);

	public void approveWorkLogsByManager(ApproveWorkLogsByEmployeeDts approveWorkLogsByEmployeeDts);

    void deleteWorkLog(WorkLogDeleteDts workLogDeleteDts);
}
