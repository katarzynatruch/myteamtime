package pl.kt.myteamtime.facade;

import pl.kt.myteamtime.dts.CapacityDts;
import pl.kt.myteamtime.dts.CapacityRetrieveDts;
import pl.kt.myteamtime.dts.GraphFilterListDts;

public interface CapacityFacade {

	CapacityDts loadGraph(CapacityRetrieveDts capacityRetrieveDts);

	GraphFilterListDts loadFilters(String managerIdentificator);

    void exportToExcel(CapacityDts capacityDts);
}
