package pl.kt.myteamtime.facade;

import pl.kt.myteamtime.dts.StatisticsComboDiagramDts;
import pl.kt.myteamtime.dts.StatisticsComboDiagramRetrieveDts;

public interface StatisticsComboFacade {

	public StatisticsComboDiagramDts loadTabData(StatisticsComboDiagramRetrieveDts statisticsComboDiagramRetrieveDts);

	void exportToExcel(StatisticsComboDiagramDts statisticsComboDiagramDts, int weekFrom, int weekTo, int yearFrom,
			int yearTo);
}