package pl.kt.myteamtime.facade;

import pl.kt.myteamtime.dts.EmployeesDts;

public interface EmployeeManagementFacade {

	public void saveOrUpdateEmployee(EmployeesDts employeesDts);

	public EmployeesDts loadView(String managerIdentificator);

    void deleteEmployee(Long employeeId);
}
