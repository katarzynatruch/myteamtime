package pl.kt.myteamtime.facade;

import pl.kt.myteamtime.dts.EmployeesWorkLogConfirmedDts;

public interface EmployeeWorkLogConfirmedFacade {

    EmployeesWorkLogConfirmedDts loadList(String managerIdentificator);


    EmployeesWorkLogConfirmedDts loadListOfLazyEmployees(String managerIdentificator);
}
