package pl.kt.myteamtime.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;

import java.util.ArrayList;
import java.util.List;

@Configuration
@EnableWebSecurity
@Profile("!https")
public class SecSecurityConfig extends WebSecurityConfigurerAdapter {

	@Autowired
	private UserDetailsService userDetailsService;
	@Autowired
	private AuthenticationManagerBuilder auth;

	@Bean
	public PasswordEncoder passwordEncoder() {
		return new BCryptPasswordEncoder(11);
	}

	@Bean
	public DaoAuthenticationProvider authProvider() {
		DaoAuthenticationProvider authProvider = new DaoAuthenticationProvider();
		authProvider.setUserDetailsService(userDetailsService);
		authProvider.setPasswordEncoder(passwordEncoder());
		return authProvider;
	}

	// ...
//    @Override
//    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
//
//	 //add our users for in memory authentication
//
//        auth.inMemoryAuthentication()
//                .withUser(users.username("john").password(encoder().encode("adminPass")).roles("EMPLOYEE"))
//                .withUser(users.username("mary").password(encoder().encode("adminPass")).roles("EMPLOYEE", "MANAGER"))
//                .withUser(users.username("susan").password(encoder().encode("adminPass")).roles("EMPLOYEE", "ADMIN"));
//    }

	@Override
	protected void configure(final HttpSecurity http) throws Exception {
//		http
//				.authorizeRequests()
//				.antMatchers("/resources/**").permitAll()
//				.anyRequest().authenticated()
//				.and()
//				.formLogin()
//				.loginPage("/login")
//				.permitAll()
//				.and()
//				.logout()
//				.permitAll();
//		http.csrf().disable().authorizeRequests();
		List<String> urlsForManager = new ArrayList<>();
		urlsForManager.add("/availability/**");
		String[] arrayOfUrlsForManager = new String[urlsForManager.size()];
		arrayOfUrlsForManager = urlsForManager.toArray(arrayOfUrlsForManager);


		http.authorizeRequests()

//				.antMatchers("/admin/**").hasAuthority("ADMIN")
//				.antMatchers("/users/**").hasAuthority("ADMIN")
//				.antMatchers(arrayOfUrlsForManager).hasAuthority("MANAGER")
//				.antMatchers("/**").hasAuthority("EMPLOYEE")
				.antMatchers("/admin/**").hasRole("ADMIN")
				.antMatchers("/users/**").hasRole("ADMIN")
				.antMatchers(arrayOfUrlsForManager).hasRole("MANAGER")
				.antMatchers("/**").hasRole("EMPLOYEE")
//				.antMatchers(HttpMethod.GET, "/index*", "/static/**", "/*.js", "/*.json", "/*.ico").permitAll()
				.antMatchers("/user/updatePassword*", "/user/savePassword*", "/updatePassword*")
				.hasAuthority("CHANGE_PASSWORD_PRIVILEGE").anyRequest().authenticated().and().csrf().disable().formLogin()
				.usernameParameter("username")
				.passwordParameter("password")
				.loginPage("http://localhost:3000/index.html").permitAll()
				.loginProcessingUrl("/perform_login").permitAll()
				.defaultSuccessUrl("http://localhost:3000/admin/about",true)
				.failureUrl("http://localhost:3000/index.html?error=true")
				.and()
				.logout()
				.logoutUrl("/perform_logout")
				.deleteCookies("JSESSIONID")
				.deleteCookies("remember-me")
				.and()
				.exceptionHandling().accessDeniedPage("/access-denied");
//				.and()
//				.rememberMe().key("fds8lsdFD$3@#$fds34SOLHGF56");
	}


	@Override
	@Bean
	public AuthenticationManager authenticationManagerBean() throws Exception {
		return super.authenticationManagerBean();
	}

	@Override
	protected void configure(AuthenticationManagerBuilder auth) throws Exception {
		auth.inMemoryAuthentication()
				.withUser("admin").password(passwordEncoder().encode("admin")).roles("ADMIN");

		this.auth = auth;
		auth.authenticationProvider(authProvider());
	}

}
