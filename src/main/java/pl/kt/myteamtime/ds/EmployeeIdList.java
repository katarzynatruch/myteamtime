package pl.kt.myteamtime.ds;

import java.util.List;

import lombok.Data;

@Data
public class EmployeeIdList {

	List<ProjectIdWithMaxHoursDs> projectIdWithMaxHoursDsList;
	private Long employeeId;

}
