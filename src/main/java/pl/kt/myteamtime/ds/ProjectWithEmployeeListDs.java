package pl.kt.myteamtime.ds;

import java.util.List;

import lombok.Data;
import pl.kt.myteamtime.dto.EmployeeDTO;
import pl.kt.myteamtime.dto.ProjectDTO;

@Data
public class ProjectWithEmployeeListDs {

	ProjectDTO project;
	List<EmployeeDTO> employees;
	
	
}
