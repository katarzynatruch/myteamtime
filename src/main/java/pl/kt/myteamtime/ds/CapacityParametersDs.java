package pl.kt.myteamtime.ds;

import java.util.List;

import lombok.Data;
import pl.kt.myteamtime.dto.ProjectDTO;

@Data
public class CapacityParametersDs {

	
	private List<ProjectDTO> projects;
	private int week;
	private int year;
	private long workedHours;
}
