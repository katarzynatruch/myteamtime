package pl.kt.myteamtime.ds;

import java.util.List;

import lombok.Data;
import pl.kt.myteamtime.dto.EmployeeDTO;

@Data
public class ProjectsAndTimeForEmployeeDs {

	private List<SingleProjectAndHoursEmployeeDs> projects;
	private EmployeeDTO employee;

}
