package pl.kt.myteamtime.ds;

import lombok.Data;
import pl.kt.myteamtime.dto.ProjectDTO;

@Data
public class ProjectIdWithMaxHoursDs {

	private Long projectId;
 	Integer maxHours;
}
