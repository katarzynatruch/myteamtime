package pl.kt.myteamtime.entity;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.Data;
import lombok.EqualsAndHashCode;
import pl.kt.myteamtime.enums.Event;

@Data
@EqualsAndHashCode(callSuper=false)
@Entity
@Table(name = "availability")
public class Availability extends AbstractEntity{
	
	@Column(name = "event")
	@Enumerated(EnumType.STRING)
	private Event event;
	@Column(name = "day")
	private Integer day;
	@Column(name = "month")
	private Integer month;
	@Column(name = "week")
	private Integer week;
	@Column(name = "year")
	private Integer year;
	
	
	@ManyToOne(cascade = { CascadeType.DETACH, CascadeType.REFRESH })
	@JoinColumn(name = "employee_id")
	private Employee employee;
	

}
