package pl.kt.myteamtime.entity;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.Data;
import lombok.EqualsAndHashCode;
import pl.kt.myteamtime.enums.ProjectMilestone;

@Data
@EqualsAndHashCode(callSuper=false)
@Entity
@Table(name = "deadline")
public class Deadline extends AbstractEntity{
	
	@Column(name = "milestone")
	@Enumerated(EnumType.STRING)
	private ProjectMilestone milestones;
	@Column(name = "week")
	private Integer week;
	@Column(name = "year")
	private Integer year;
	@Column(name="comment")
	private String comment;
	
	// Relations
	
	@ManyToOne(cascade = { CascadeType.DETACH, CascadeType.REFRESH })
	@JoinColumn(name = "project_id")
	private Project project;

}
