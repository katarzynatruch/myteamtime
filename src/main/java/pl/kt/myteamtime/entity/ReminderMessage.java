package pl.kt.myteamtime.entity;

import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.*;

@Data
@EqualsAndHashCode(callSuper=false)
@Entity
@Table(name = "message")
public class ReminderMessage extends AbstractEntity {

    @Column(name = "defaultReminderMessage")
    private String defaultReminderMessage;

    @OneToOne(cascade = { CascadeType.DETACH, CascadeType.REFRESH })
    @JoinColumn(name = "manager_id")
    private Manager manager;
}
