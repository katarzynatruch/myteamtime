package pl.kt.myteamtime.entity;

import java.util.List;

import javax.persistence.*;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
@Entity
@Table(name = "Employee")
public class Employee extends AbstractEntity {

	@Column(name = "first_name")
	private String firstName;
	@Column(name = "last_name")
	private String lastName;
	@Column(name = "email")
	private String email;
	@Column(name = "identificator", unique = true)
	private String identificator;
	@Column(name = "is_active")
	private Boolean isActive;

	// Relations

	@ManyToMany(fetch = FetchType.LAZY, cascade = { CascadeType.DETACH, CascadeType.REFRESH })
	@JoinTable(name = "employee_team", joinColumns = @JoinColumn(name = "employee_id"), inverseJoinColumns = @JoinColumn(name = "team_id"))
	private List<Team> teams;

	@ManyToMany(fetch = FetchType.LAZY, cascade = { CascadeType.DETACH, CascadeType.REFRESH })
	@JoinTable(name = "project_employee_max_hours", joinColumns = @JoinColumn(name = "employee_id"), inverseJoinColumns = @JoinColumn(name = "project_id"))
	private List<Project> projects;

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "employee", cascade = { CascadeType.DETACH, CascadeType.REFRESH })
	private List<ProjectEmployeeMaxHours> listOfProjectEmployeeMaxHours;

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "employee")
	private List<WorkLog> workLogs;

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "employee", cascade = { CascadeType.DETACH, CascadeType.REFRESH })
	private List<Availability> availabilityList;

}
