package pl.kt.myteamtime.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import pl.kt.myteamtime.entity.PasswordResetToken;

public interface PasswordTokenRepository extends JpaRepository<PasswordResetToken, Long> {
	PasswordResetToken findByToken(String token);
}
