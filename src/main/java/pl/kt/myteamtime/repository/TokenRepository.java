package pl.kt.myteamtime.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import pl.kt.myteamtime.entity.User;
import pl.kt.myteamtime.entity.VerificationToken;

public interface TokenRepository extends JpaRepository<VerificationToken, Long> {

    @Query("select t from VerificationToken t where t.token=:token")
    VerificationToken findByToken(@Param("token") String token);

    @Query("select t from VerificationToken t where t.user=user")
    VerificationToken findByUser(@Param("user") User user);
}
