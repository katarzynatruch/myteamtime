package pl.kt.myteamtime.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import pl.kt.myteamtime.entity.Manager;

@Repository
public interface ManagerRepository extends JpaRepository <Manager, Long>{

    @Query("select m from Manager m where m.identificator=:identificator")
    Manager findByIdentificator(@Param("identificator")String managerIdentificator);
}
