package pl.kt.myteamtime.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import pl.kt.myteamtime.entity.User;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {


    @Query("select u from User u where u.email=:email and active=true")
    User findActiveByEmail(@Param("email")String email);

    @Query("select u from User u where u.identificator=:identificator and active=true")
    User findActiveByIdentificator(@Param("identificator")String identificator);
}
