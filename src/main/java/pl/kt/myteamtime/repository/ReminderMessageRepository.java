package pl.kt.myteamtime.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import pl.kt.myteamtime.entity.ReminderMessage;

import java.util.List;

public interface ReminderMessageRepository extends JpaRepository<ReminderMessage, Long> {

	@Query("select r from ReminderMessage r join r.manager where r.manager.identificator=:managerIdentificator")
    ReminderMessage findByManagerIdentificator(@Param("managerIdentificator") String managerIdentificator);

	@Query("select r from ReminderMessage r join r.manager where r.manager.identificator=:managerIdentificator and r" +
            ".defaultReminderMessage=:defaultReminderMessage")
    List<ReminderMessage> findByManagerIdentificatorAndMessage(@Param("managerIdentificator") String managerIdentificator,
			@Param("defaultReminderMessage") String defaultReminderMessage);
}
