package pl.kt.myteamtime.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import pl.kt.myteamtime.entity.Employee;
import pl.kt.myteamtime.entity.Project;
import pl.kt.myteamtime.entity.ProjectEmployeeMaxHours;

@Repository
public interface ProjectEmployeeMaxHoursRepository extends JpaRepository <ProjectEmployeeMaxHours, Long>{

	
	@Query("select h from ProjectEmployeeMaxHours h where h.employee=:employee and h.project=:project")
	ProjectEmployeeMaxHours findByEmployeeAndProject(@Param("employee")Employee employee,@Param("project") Project project);


}
