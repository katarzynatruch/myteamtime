package pl.kt.myteamtime.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import pl.kt.myteamtime.entity.Employee;

@Repository
public interface EmployeeRepository extends JpaRepository<Employee, Long> {

	@Query("select e from Employee e join e.teams t where t.id =:teamsId and e.isActive = true")
	List<Employee> findActiveEmployeesByTeamsId(@Param("teamsId") Long teamsId);

	@Query("select e from Employee e where e.identificator =:employeeIdentificator and e.isActive = true")
	Employee findActiveByIdentificator(@Param("employeeIdentificator") String employeeIdentificator);
	// Optional<Employee> findByIdentificator(String identificator);

	@Query("select e.id from Employee e where e.identificator =:employeeIdentificator and e.isActive = true")
	Long findIdByIdentificator(@Param("employeeIdentificator") String employeeIdentificator);

	@Query("select e from Employee e join e.teams t where t.manager.identificator=:managerIdentificator and e.isActive=true")
	List<Employee> findActiveByManagerIdentificator(@Param("managerIdentificator") String managerIdentificator);

	@Query("select e from Employee e join e.teams t join e.projects p where t.manager.identificator =:managerIdentificator and p.id =:projectId and e.isActive = true")
	List<Employee> findActiveByProjectAndManagerIdentificator(@Param("projectId") Long id,
															  @Param("managerIdentificator") String managerIdentificator);

	@Query("select e from Employee e where e.id=:employeeId and e.isActive=true")
	Optional<Employee> findActiveById(@Param("employeeId") Long employeeId);
}
