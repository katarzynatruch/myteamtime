package pl.kt.myteamtime.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import pl.kt.myteamtime.entity.Deadline;

@Repository
public interface DeadlineRepository extends JpaRepository <Deadline, Long>{

	
}