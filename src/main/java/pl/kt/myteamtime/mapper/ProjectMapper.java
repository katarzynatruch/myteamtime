package pl.kt.myteamtime.mapper;

import java.util.List;

import org.mapstruct.Context;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;

import pl.kt.myteamtime.dto.ProjectDTO;
import pl.kt.myteamtime.entity.Project;

@Mapper(uses = { EmployeeMapper.class }, componentModel = "spring")
public abstract class ProjectMapper {

	public abstract Project mapToProject(ProjectDTO projectDTO, @MappingTarget Project project);

	// @InheritInverseConfiguration(name = "mapToProject")
	// @Mapping(ignore = true, target = "workLogs")
	public abstract ProjectDTO mapToProjectDTO(Project project, @Context CycleAvoidingMappingContext context);

	@Mapping(ignore = true, target = "workLogs")
	public abstract Project mapToProject(ProjectDTO projectDTO, @Context CycleAvoidingMappingContext context);

	// @Mapping(ignore = true, target = "workLogs")
	public abstract List<ProjectDTO> mapToProjectListToProjectDTOList(List<Project> projects,
			@Context CycleAvoidingMappingContext context);

	// @Mapping(ignore = true, target = "workLogs")
	public abstract List<Project> mapToProjectListToProjectList(List<ProjectDTO> projectsDTO,
			@Context CycleAvoidingMappingContext context);
}
