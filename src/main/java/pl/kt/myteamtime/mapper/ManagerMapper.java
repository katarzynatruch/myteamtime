package pl.kt.myteamtime.mapper;

import org.mapstruct.Context;
import org.mapstruct.Mapper;
import org.mapstruct.MappingTarget;

import pl.kt.myteamtime.dto.ManagerDTO;
import pl.kt.myteamtime.entity.Manager;

@Mapper(uses = {TeamMapper.class}, componentModel = "spring")
public abstract class ManagerMapper {


	public abstract Manager mapToManager(ManagerDTO managerDTO, @MappingTarget Manager manager);
	
	public abstract Manager mapToManager(ManagerDTO managerDTO, @Context CycleAvoidingMappingContext context);
	
	public abstract ManagerDTO mapToManagerDTO(Manager manager, @Context CycleAvoidingMappingContext context);
	
	
	

}
